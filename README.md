<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need:

```sh
 gem install rails
```

```sh
 Docker
```

### Setup

Clone this repository to your desired folder:

```sh
  git clone git@gitlab.com:oshie_15/isv3-106-building-a-restful-api-with-rails.git

```

```sh
  cd isv3-106-building-a-restful-api-with-rails
  and switch to the correct branch name `restful_api`
```

### Usage

To run the project, execute the following command:

```sh
  docker-compose build
```

```sh
  docker-compose up -d
```

## 👥 Authors <a name="Oshie" />

👤 **Oshie**

- GitLab: [GitLab Profile](https://gitlab.com/oshie_15)
